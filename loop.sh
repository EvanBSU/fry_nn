for j in $(seq 4000 1000 4000)
do for i in $(seq 0.6 0.1 0.6)
do echo $i $j
f=$i
steps=$j

echo \#!/bin/bash -l > $f-$steps-run.sh
echo \#SBATCH -N 1 >> $f-$steps-run.sh
echo \#SBATCH -n 16 >> $f-$steps-run.sh
echo \#SBATCH --partition=nvlink >> $f-$steps-run.sh
echo \#SBATCH -o /home/erjank_project/evanmiller326/Neural_Network/runs/$f-$steps-run.o >> $f-$steps-run.sh
echo \#SBATCH -t 36:00:00 >> $f-$steps-run.sh
echo \#SBATCH --gres=gpu:4 >> $f-$steps-run.sh
echo cd /home/erjank_project/evanmiller326/Neural_Network >> $f-$steps-run.sh
echo echo I am trying f=$f steps=$steps >> $f-$steps-run.sh
echo python3 fry_nn/signac_NN.py $f $steps >> $f-$steps-run.sh
echo echo Plotting the Phase Diagrams >> $f-$steps-run.sh
echo python3 fry_nn/plot_city.py $f $steps >> $f-$steps-run.sh
echo echo Creating the Comparison >> $f-$steps-run.sh
echo python3 fry_nn/acc_hist.py $f $steps >> $f-$steps-run.sh
echo mv $f-$steps-run.sh runs/ >> $f-$steps-run.sh
sbatch $f-$steps-run.sh
done
done

#for j in $(seq 4000 1000 4000)
#do for i in $(seq 0.6 0.1 0.7)
#do echo $i $j
#f=$i
#steps=$j
#done 
#done
