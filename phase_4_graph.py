import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
from multiprocessing import Pool
import time

datum = {0.:'mo', 1.:'ro', 2.:'co', 3.:'bo', 4.:'go'}

def clear_texts():
    for model_name in models:
        for molecule_name in molecules:
            if os.path.isfile(folder+'{}_{}.txt'.format(model_name, molecule_name)):
                os.remove(folder+'{}_{}.txt'.format(model_name, molecule_name))

def write(lst):
    mol = lst[3]
    model = lst[4]
    a, b, c = float(lst[0]), float(lst[1]), float(lst[2])
    with open(folder+'{}_{}.txt'.format(model, mol), "a") as myfile:
        myfile.write("{} {} {}\n".format(a, b, c))
        
def choose_system(array, counter, model_name, molecule_name):
    plots = [sub1, sub2, sub3, sub4]
    markers = [datum[array[x,2]] for x in range(len(array[:,2]))]
    for x, y, m in zip(array[:,0], array[:,1], markers):
        plots[counter].plot(x, y, m, markersize = 1, alpha = 0.5)
    plots[counter].set_title('{} {}'.format(model_name, molecule_name), fontsize = 18)
    print("Just finished {} {}".format(model_name, molecule_name))
    
def plot_data():
    counter = 0
    for model_name in models:
        for molecule_name in molecules:
            with open(folder+'{}_{}.txt'.format(model_name, molecule_name)) as f:
                 diagram_data= f.readlines()
                 diagram_data = [x.strip().split() for x in diagram_data]
                 diagram_data = np.array(diagram_data, dtype = float)
                 choose_system(diagram_data, counter, model_name, molecule_name)
            counter+=1
    plt.savefig('/home/erjank_project/evanmiller326/Neural_Network/diagram.pdf')

start_time = time.time()

models = ['Flexible', 'Rigid']
molecules = ['Perylene', 'Perylothiophene']

folder = '/home/erjank_project/evanmiller326/Neural_Network/'
if os.path.isfile('/home/erjank_project/evanmiller326/Neural_Network/test.txt'):
    with open('/home/erjank_project/evanmiller326/Neural_Network/test.txt') as f:
        content = f.readlines()
content = [x.strip().split() for x in content]

clear_texts()

#for i in content:
#    pick_plot(i)
start_time = time.time()

with Pool() as pool:
    pool.map(write, content)

run_time = (time.time()-start_time)/60
print("It took {:.2f} minutes to separate the data.".format(run_time))

start_time = time.time()

fig = plt.figure(figsize = (12, 12))
sub1 = fig.add_subplot(2,2,1)
sub2 = fig.add_subplot(2,2,2)
sub3 = fig.add_subplot(2,2,3)
sub4 = fig.add_subplot(2,2,4)

plot_data()

#sub1.set_title('Rigid Perylene')
#sub2.set_title('Flexible Perylene')
#sub3.set_title('Rigid Perylothiophene')
#sub4.set_title('Flexible Perylothiophene')

run_time = (time.time()-start_time)/60
print("It took {:.2f} minutes to create the graph.".format(run_time))
