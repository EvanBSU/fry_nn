import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from multiprocessing import Pool
import os
import sys

def plot_the_data(lst):
    acc, metric, result, system = lst[1], lst[2], lst[3], lst[0]
    data = np.loadtxt(run_dir+system+"_"+metric+"_"+result+".txt")
    Ts = add_noise(data[:,3], 0.2)
    phis = add_noise(data[:,2], 0.1)
    real = data[:,1]
    NN = data[:,0]
    plot_data(real, NN, phis, Ts, metric, result, system, acc)

def add_noise(array, scale):
    noise = np.random.uniform(0, 1, len(array))
    noise -= 0.5
    noise *= scale
    w_noise = array+noise
    return w_noise

def plot_data(real, NN, phis, Ts, metric, result, system, acc):
    plt.close()
    fig = plt.figure(figsize = (12, 12))
    sub1 = fig.add_subplot(2,1,1)
    sub1.set_ylabel("Actual")
    for i in range(len(real)):
        if real[i] != NN[i]:
            sub1.plot(phis[i], Ts[i], datum[real[i]], markersize=4, alpha = 1.0)
        else:
            sub1.plot(phis[i], Ts[i], datum[real[i]], markersize=4, alpha = 0.3)
    sub1.xaxis.set_ticks([])
    sub2 = fig.add_subplot(2,1,2)
    sub2.set_ylabel("Neural Net")
    for i in range(len(NN)):
        if NN[i] != real[i]:
            sub2.plot(phis[i], Ts[i], datum[NN[i]], markersize=4, alpha = 1.0)
        else:
            sub2.plot(phis[i], Ts[i], datum[NN[i]], markersize=4, alpha = 0.3)
    sub2.text(0.5, np.amax(Ts)+np.amax(Ts)*0.1, "{:.2f}% Accurate".format(float(acc)*100))
    fig.savefig(run_dir+var_dir+file_dir+"{}-{}-{}.pdf".format(system, metric, result))

datum = {0.:'mo', 1.:'ro', 2.:'co', 3.:'bo', 4.:'go'}

run_dir = '/home/erjank_project/evanmiller326/Neural_Network/T_phi_data_files/'

f = sys.argv[1]
steps = sys.argv[2]

if os.path.isdir(run_dir + 'f{}_steps{}/'.format(f, steps)) is False:
    os.mkdir(run_dir + 'f{}_steps{}/'.format(f, steps))

data_sets = [line.split() for line in open(run_dir+'comparison.txt', 'r')]

var_dir = 'f{}_steps{}/'.format(f, steps)

file_dir = 'highlight/'
if os.path.isdir(run_dir+var_dir+file_dir) is False:
    os.mkdir(run_dir+var_dir+file_dir)

screened = [line for line in data_sets if line[0] != 'all']
with Pool() as pool:
    pool.map(plot_the_data, screened)
#for line in data_sets:
#    print(line)
#    if line[0] != 'all':
#        plot_the_data(acc = line[1], system = line[0], metric = line[2], result = line[3])

