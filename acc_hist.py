import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os
import shutil
import sys

def plot_data(metric, criteria, systems, accuracies):
    plt.close()
    x_pos = np.arange(len(systems))
    barlist = plt.bar(x_pos, accuracies, align='center', alpha = 0.5)
    for x, y in zip(x_pos, accuracies):
        plt.text(x, y, "{:.2f}".format(y), ha = 'center', fontsize = 15)
    barlist[0].set_color('r')
    barlist[1].set_color('k')
    barlist[2].set_color('g')
    barlist[3].set_color('c')
    barlist[4].set_color('b')
    plt.xticks(x_pos, systems)
    plt.ylim([0,100])
    plt.ylabel('% Accuracy', fontsize = 12)
    plt.title("{} to distinguish between {}".format(metric, criteria), fontsize = 12)
    plt.savefig(run_dir+var_dir+file_dir+"{}-{}.pdf".format(metric, criteria))

def parse(metric, criteria):
    systems = []
    accuracies = []
    for run in range(len(data_list)):
        if data_list[run][2] == metric and data_list[run][3] == criteria:
            systems.append(data_list[run][0])
            accuracies.append(100*float(data_list[run][1]))
    return systems, accuracies

run_dir = '/home/erjank_project/evanmiller326/Neural_Network/T_phi_data_files/'

f = sys.argv[1]
steps = sys.argv[2]

if os.path.isdir(run_dir + 'f{}_steps{}/'.format(f, steps)) is False:
    os.mkdir(run_dir + 'f{}_steps{}/'.format(f, steps))

var_dir = 'f{}_steps{}/'.format(f, steps)

data_list = [line.split() for line in open(run_dir+var_dir+'comparison.txt', 'r')]

file_dir = 'data_hists/'
if os.path.isdir(run_dir+var_dir+file_dir) is False:
    os.mkdir(run_dir+var_dir+file_dir)

system = list(set([i[0] for i in data_list]))
metrics = list(set([i[2] for i in data_list]))
criteria = list(set([i[3] for i in data_list]))

for i in metrics:
    for j in criteria:
        systems, accuracies = parse(i, j)
        plot_data(i, j, systems, accuracies)
